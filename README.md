## Introduction

FLTK Free Software with radio streams from the web.

Soft: 
flstreamtuner and flsound. 

## Screenshot 



![](medias/1650280491-1-flstreamtuner.png)

## Installation 

````

 apt-get install -y    libfltk1.3-dev g++ clang make mpg123 

 apt-get install -y  amixer 

 c++   flstreamtuner.cxx  -lX11 -lfltk -o  flstreamtuner 

 cp flstreamtuner /usr/local/bin/flstreamtuner 
 

````




## Running 

````

  flstreamtuner 

````



## Stream Database/URL

raw file, which can be edited: 
https://gitlab.com/openbsd98324/flstreamtuner/-/raw/main/src/flstreamtuner.fld


## Website

http://gitlab.com/openbsd98324/flstreamtuner



